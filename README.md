# Transfer learning for enabling quality predictions in small batch production

Source code of the submittedpublication "Transfer learning for enabling quality predictions in small batch production" by 
Klasen et al. (Laboratory for Machine Tools and Production Engineering, WZL of RWTH Aachen University) at CIRP ICME '24.



## Information
This repository contains a small excerpt of a bigger project. It is intended to make the procedure described in the 
paper comprehensible. The full framework will be published at a later state.

## License
The code is subject to MIT license.

